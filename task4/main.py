import numpy as np

def calculate_joint_entropy(matrix):
    # Вычисляем совместную вероятность для всех пар событий A и B
    joint_probabilities = matrix / np.sum(matrix)
    joint_entropy = -np.sum(joint_probabilities * np.log2(joint_probabilities + (joint_probabilities == 0)))
    return joint_entropy

def calculate_entropy_of_events(matrix):
    # Вычисляем энтропию отдельно для каждого события A и B
    probabilities_A = np.sum(matrix, axis=1) / np.sum(matrix)
    probabilities_B = np.sum(matrix, axis=0) / np.sum(matrix)
    entropy_A = -np.sum(probabilities_A * np.log2(probabilities_A + (probabilities_A == 0)))
    entropy_B = -np.sum(probabilities_B * np.log2(probabilities_B + (probabilities_B == 0)))
    return entropy_A, entropy_B

def calculate_conditional_entropy(matrix):
    # Вычисляем условную энтропию события B при условии A
    # H(B|A) = H(A,B) - H(A)
    joint_entropy_AB = calculate_joint_entropy(matrix)
    entropy_A, _ = calculate_entropy_of_events(matrix)
    conditional_entropy_B_given_A = joint_entropy_AB - entropy_A
    return conditional_entropy_B_given_A

def calculate_mutual_information(matrix):
    # Вычисляем взаимную информацию между A и B
    # I(A;B) = H(A) + H(B) - H(A,B)
    entropy_A, entropy_B = calculate_entropy_of_events(matrix)
    joint_entropy_AB = calculate_joint_entropy(matrix)
    mutual_information = entropy_A + entropy_B - joint_entropy_AB
    return mutual_information

def task():
    # Предполагаемая матрица вероятностей для двух событий A и B
    # Это пример и должна быть заменена на реальные данные при их наличии
    matrix = np.array([[0.1, 0.09, 0.01],
                       [0.08, 0.07, 0.05],
                       [0.03, 0.32, 0.25]])
    
    # Вычисляем запрошенные значения
    H_AB = calculate_joint_entropy(matrix)
    H_A, H_B = calculate_entropy_of_events(matrix)
    Ha_B = calculate_conditional_entropy(matrix)
    I_AB = calculate_mutual_information(matrix)
    
    # Формируем итоговый список и округляем значения до второго знака
    results = [round(value, 2) for value in [H_AB, H_A, H_B, Ha_B, I_AB]]
    return results

# Вызываем функцию task и получаем результат
result_list = task()
print(result_list)
