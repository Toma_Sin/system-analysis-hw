import numpy as np
import json

def get_transposed_matrix(matrix):
    """Транспонируем матрицу отношений."""
    return np.transpose(matrix)

def multiply_matrices(matrix1, matrix2):
    """Поэлементно перемножаем матрицы."""
    return np.multiply(matrix1, matrix2)

def logical_sum_matrices(matrix1, matrix2):
    """Логическое сложение двух матриц."""
    return np.logical_or(matrix1, matrix2)

def convert_ranking_to_relation_matrix(ranking):
    """Преобразуем ранжировку в матрицу отношений."""
    unique_elements = set()
    for group in ranking:
        if isinstance(group, list):
            unique_elements.update(group)
        else:
            unique_elements.add(group)
    n = max(unique_elements)

    relation_matrix = np.zeros((n, n), dtype=int)
    for group in ranking:
        if not isinstance(group, list):
            group = [group]
        for i in group:
            for j in group:
                relation_matrix[i-1, j-1] = 1

    return relation_matrix

def find_contradictions(y_a, y_b, y_a_t, y_b_t):
    """Поиск противоречий между ранжировками."""
    contradictions = np.logical_and(y_a != y_b, y_a_t != y_b_t)
    return np.where(contradictions)

def create_agreement_ranking(rank_a, rank_b, contradictions):
    contradictions_set = set(map(tuple, np.transpose(contradictions)))
    agreement_ranking = []
    for item in rank_a + rank_b:
        if isinstance(item, list):
            cluster = [x for x in item if (x-1, x-1) not in contradictions_set]
            if len(cluster) > 1:
                agreement_ranking.append(cluster)
            elif len(cluster) == 1:
                agreement_ranking.append(cluster[0])
        elif (item-1, item-1) not in contradictions_set:
            agreement_ranking.append(item)

    seen = set()
    clean_ranking = []
    for elem in agreement_ranking:
        if isinstance(elem, list):
            clean_elem = tuple(sorted(elem))
            if clean_elem not in seen:
                seen.add(clean_elem)
                clean_ranking.append(list(clean_elem))
        elif elem not in seen:
            seen.add(elem)
            clean_ranking.append(elem)
            
    return clean_ranking  # This is the added return statement

def task(json_rank_a, json_rank_b):
    rank_a = json.loads(json_rank_a)
    rank_b = json.loads(json_rank_b)

    y_a = convert_ranking_to_relation_matrix(rank_a)
    y_b = convert_ranking_to_relation_matrix(rank_b)

    y_a_t = get_transposed_matrix(y_a)
    y_b_t = get_transposed_matrix(y_b)

    y_ab = multiply_matrices(y_a, y_b)
    y_ab_t = multiply_matrices(y_a_t, y_b_t)

    contradictions_matrix = logical_sum_matrices(y_ab, y_ab_t)

    contradictions = find_contradictions(y_a, y_b, y_a_t, y_b_t)

    agreement_ranking = create_agreement_ranking(rank_a, rank_b, contradictions)

    return json.dumps(agreement_ranking)

# Test the function
json_rank_a = json.dumps([1, [2, 3], 4, [5, 6, 7], 8, 9, 10])
json_rank_b = json.dumps([[1, 2], [3, 4, 5], 6, 7, 9, [8, 10]])
agreed_ranking = task(json_rank_a, json_rank_b)
print(agreed_ranking)