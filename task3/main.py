import numpy as np
import pandas as pd

def calculate_entropy(matrix):
    # Расчет вероятностей каждого узла как частотное распределение
    probabilities = np.sum(matrix, axis=1) / np.sum(matrix)
    # Фильтрация нулевых вероятностей для предотвращения деления на ноль
    probabilities = probabilities[probabilities > 0]
    # Расчет энтропии
    entropy = -np.sum(probabilities * np.log2(probabilities))
    return entropy

def task(csv_data):
    # Преобразование CSV строки в массив
    data = np.array([[int(num) for num in row.split(',')] for row in csv_data.strip().split('\n')])
    # Удаление колонки с идентификаторами узлов, если она присутствует
    if data.shape[1] == 6:
        data = data[:, 1:]
    # Вычисление энтропии
    entropy = calculate_entropy(data)
    # Округление до одного знака после запятой
    entropy_rounded = round(entropy, 1)
    return entropy_rounded

file_path_task3 = './task3.csv'
data_task3 = pd.read_csv(file_path_task3)

# Преобразуем DataFrame обратно в CSV формат без заголовков и индекса для функции task
csv_input_task3 = data_task3.to_csv(header=False, index=False).strip()

# Использование функции task для вычисления энтропии графа
entropy_result = task(csv_input_task3)
print(entropy_result)