import pandas as pd

def main():
    # Создаем парсер аргументов
    parser = argparse.ArgumentParser(description='Retrieve a cell value from a CSV file.')
    # Добавляем аргументы
    file_path = './task1.csv'
    row = 2
    column = 1
    
    # Парсим аргументы
    args = parser.parse_args()
    
    # Читаем CSV файл
    data = pd.read_csv(file_path)
    
    # Получаем значение ячейки
    # Предполагается, что пользователь вводит номера строк и столбцов, начиная с 1,
    # поэтому отнимаем 1 для индексации с 0
    cell_value = data.iloc[row - 1, column - 1]
    
    # Выводим значение ячейки
    print(cell_value)

if __name__ == "__main__":
    main()
