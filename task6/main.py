from scipy.stats import kendalltau
import json

def task(json_rank_a, json_rank_b):
    # Десериализация JSON-строк в списки
    rank_a = json.loads(json_rank_a)
    rank_b = json.loads(json_rank_b)

    # Преобразование кластеризованных ранжировок в обычные списки
    def flatten_ranking(ranking):
        flat_rank = []
        for element in ranking:
            if isinstance(element, list):
                flat_rank.extend(element)
            else:
                flat_rank.append(element)
        return flat_rank
    
    # Получение "плоских" ранжировок
    flat_rank_a = flatten_ranking(rank_a)
    flat_rank_b = flatten_ranking(rank_b)

    # Вычисление коэффициента Кендалла
    tau, _ = kendalltau(flat_rank_a, flat_rank_b)
    
    # Округление до двух знаков после запятой
    tau_rounded = round(tau, 2)
    
    return tau_rounded

# Пример использования функции
json_rank_a = json.dumps([1, [2, 3], 4, [5, 6, 7], 8, 9, 10])
json_rank_b = json.dumps([[1, 2], [3, 4, 5], 6, 7, 9, [8, 10]])

# Вызов функции для получения коэффициента Кендалла
kendall_coef = task(json_rank_a, json_rank_b)
print(kendall_coef)