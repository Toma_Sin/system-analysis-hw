import pandas as pd


def calculate_relationships(edges):
    # Создаем словарь для хранения отношений
    nodes = {node: {'r1': 0, 'r2': 0, 'r3': 0, 'r4': 0, 'r5': 0} for edge in edges for node in edge}
    
    # Заполняем отношение непосредственного управления (r1) и подчинения (r2)
    for parent, child in edges:
        nodes[parent]['r1'] += 1
        nodes[child]['r2'] += 1

    # Заполняем отношения опосредованного управления (r3) и подчинения (r4)
    # А также сотрудничества на одном уровне (r5)
    for node in nodes:
        # Получаем всех детей текущего узла
        children = [edge[1] for edge in edges if edge[0] == node]
        # Для каждого ребенка узла найдем его детей и так далее
        def find_descendants(node):
            return [edge[1] for edge in edges if edge[0] == node]

        descendants = []
        # Используем очередь для обхода дерева
        queue = children[:]
        while queue:
            current_node = queue.pop(0)
            current_descendants = find_descendants(current_node)
            descendants.extend(current_descendants)
            queue.extend(current_descendants)

        nodes[node]['r3'] = len(descendants)
        # Для r4, мы находим родительские узлы текущего узла, и делаем то же самое
        parents = [edge[0] for edge in edges if edge[1] == node]
        ancestors = []
        queue = parents[:]
        while queue:
            current_node = queue.pop(0)
            current_parents = [edge[0] for edge in edges if edge[1] == current_node]
            ancestors.extend(current_parents)
            queue.extend(current_parents)

        nodes[node]['r4'] = len(set(ancestors))  # Используем set для удаления дубликатов

        # Вычисляем r5, количество сотрудников на одном уровне
        # Будем считать, что узлы на одном уровне, если у них общий непосредственный родитель
        siblings = set()
        for parent in parents:
            siblings.update([edge[1] for edge in edges if edge[0] == parent])
        # Удаляем текущий узел из множества братьев и сестер
        siblings.discard(node)
        nodes[node]['r5'] = len(siblings)
    
    return nodes

def task(csv_data):
    # Преобразуем входную строку в список кортежей, представляющих ребра
    edges = []
    for row in csv_data.strip().split('\n'):
        source, target = map(int, row.split(','))
        edges.append((source, target))
    
    # Вычисляем отношения
    relationships = calculate_relationships(edges)
    
    # Формируем выходную строку CSV
    output = []
    for node, rel in relationships.items():
        output.append([node] + list(rel.values()))

    # Сортируем по узлам для удобства
    output.sort(key=lambda x: x[0])
    
    # Преобразуем в CSV формат
    csv_output = '\n'.join(','.join(map(str, row)) for row in output)
    
    return csv_output

# Тестирование функции
# Преобразуем DataFrame обратно в CSV формат без заголовков и индекса
file_path = './task2.csv'
csv_input = pd.read_csv(file_path)
csv_string = csv_input.to_csv(header=False, index=False)
result_csv = task(csv_string.strip())
print(result_csv)
